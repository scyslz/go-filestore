package main

import (
	"filestore/handler"
	"net/http"
)

func main() {

	// 静态资源处理
	// http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(assets.AssetFS())))
	http.Handle("/static/",
		http.StripPrefix("/static/", http.FileServer(http.Dir("./static"))))

	// 用户相关接口
	http.HandleFunc("/", handler.LoginHandler)
	http.HandleFunc("/user/signup", handler.RegisterHandler)
	http.HandleFunc("/user/signin", handler.LoginHandler)
	//http.HandleFunc("/user/info", handler.HTTPInterceptor(handler.UserInfoHandler))

	// 文件存取接口
	http.HandleFunc("/file/upload", handler.HTTPInterceptor(handler.Upload))
	//http.HandleFunc("/file/upload/suc", handler.HTTPInterceptor(handler.UploadSuc))
	http.HandleFunc("/file/meta", handler.HTTPInterceptor(handler.GetFileMetaList))
	//http.HandleFunc("/file/query", handler.HTTPInterceptor(handler.FileQueryHandler))
	//http.HandleFunc("/file/download", handler.HTTPInterceptor(handler.DownloadHandler))
	//http.HandleFunc("/file/update", handler.HTTPInterceptor(handler.FileMetaUpdateHandler))
	//http.HandleFunc("/file/delete", handler.HTTPInterceptor(handler.FileDeleteHandler))

	http.ListenAndServe("0.0.0.0:9090", nil)
}
